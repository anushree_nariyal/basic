import { Route, Switch } from 'react-router-dom';

import AllCourses from './Pages/AllCourses';
import CurrentCourses from './Pages/CurrentCourses';
import Profile from './Pages/Profile';
import Home from './Pages/Home';
import MainNavigation from './Componentt/Layout/MainNavigation';


function App() {
  return (
    <div>
      <MainNavigation/>
      <Switch>
        <Route path='/' exact>
          <Home />
          </Route>
        <Route path='/all-courses' >
          <AllCourses />
        </Route>
        <Route path='/current-courses'>
          <CurrentCourses />
        </Route>
        <Route path='/profile'>
          <Profile />
        </Route>
      </Switch>
    </div>
  );
}

export default App;