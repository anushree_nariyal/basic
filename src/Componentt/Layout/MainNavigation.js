import { Link } from 'react-router-dom';

import classes from './MainNavigation.module.css';

function MainNavigation() {
  return (
    <header className={classes.header}>
      <div className={classes.logo}>AppName</div>
      <nav>
        <ul>
          <li>
            <Link to='/'>Home</Link>
          </li> 
          <li>
            <Link to='/all-courses'>All Courses</Link>
          </li>
          <li>
            <Link to='/current-courses'>Current Courses</Link>
          </li>
          <li>
            <Link to='/Profile'>My Profile</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default MainNavigation;